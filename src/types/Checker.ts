import { Color } from "checkers_core/build-wasm/checkers_core";

export interface Checker {
  position: number;
  color: Color;
  isKing: boolean;
  x: number;
  y: number;
  id: string;
}
