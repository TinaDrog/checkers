export interface Cell {
  x: number;
  y: number;
  index: number;
  color: 'light' | 'dark';
}
