const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData:
          '@import "src/assets/styles/base/fonts.scss";' +
          '@import "src/assets/styles/base/colors.scss";' +
          '@import "src/assets/styles/base/icons.scss";' +
          '@import "src/assets/styles/base/core.scss";' +
          '@import "src/assets/styles/base/classes.scss";' +
          '@import "src/assets/styles/utils/mixins.scss";' +
          '@import "src/assets/styles/utils/animations.scss";'
      }
    }
  },
})
